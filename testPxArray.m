D = dir( 'reg*.mat' );
G = csvread( './train/p.csv' );
r = zeros( length( D ), 1 );
E = cell( size( r ) );
n = cell( size( r ) );
featCell = cell( size( r ) );

for j = 1:length( D )
  load( D(j).name );
  n{j} = N{ find( e == min( e ) ) };
  featCell{j} = feat;
  [r(j),E{j}] = testPx( N{ find( e == min( e ) ) }, feat, G );
end

r

save testPxArray r E n featCell
