Clarity Regressor is a machine learning algorithm geared towards providing aesthetic ratings on images.

### Dependencies ###
* Ruby Dependencies
* Matlab

### How to use Clarity Regressor? ###

1. Run *500px.rb* script in the command line to scrape the "Fresh this Week" images from 500px
1. Run *readImageArray.m* to read images into .mat files
1. Run *basePx.m* to obtain base RMS for each dataset
1. Run *trainPx.m* to train an array of neural networks using the specified architecture
1. Use *testPx.m* to test the performance of the trained network on a validation set of images