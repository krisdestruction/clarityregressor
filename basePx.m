% Calculate RMS Baseline for guessing
load train;
rms( F - mean( F ) )

trainMean = mean( F );

load test;
rms( F - mean( F ) )

rms( F - trainMean )
