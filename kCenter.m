function r = kCenter( HSV )
    % Dim
    px = size( HSV, 1 ) * size( HSV, 2 );
    
    % Bag vectors
    A = squeeze( reshape( HSV, [], px, 3) );

    % K means of HSV vectors
    [kLab,kC] = kmeans( A, 5 );

    % Frequency of histogram
    kFreq = histc( kLab, unique( kLab ) );
    % Colourfulness of centroids
    c = kC(:);

    % Filter low freq centroids
    kC = kC( kFreq > px / 100, : );
    kFreq = kFreq( kFreq > px / 100 );

    % Number of centroids
    n = length( kC );
    
    r = [c; n];
end
