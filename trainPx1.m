load train;

% Create Feature Mapping
feat = @( hsv, hsvCent ) [
        kCenter( hsv );

        %mean( colvec( hsv(:,:,1) ) )';
        %mean( colvec( hsv(:,:,2) ) )';
        %mean( colvec( hsv(:,:,3) ) )';
        %var( colvec( hsv(:,:,1) ) )';
        %var( colvec( hsv(:,:,2) ) )';
        %var( colvec( hsv(:,:,3) ) )';
        hist( colvec( hsv(:,:,1) ), 7 )';
        %hist( colvec( hsv(:,:,2) ) )';
        %hist( colvec( hsv(:,:,3) ) )';
        
        %mean( colvec( hsvCent(:,:,1) ) )';
        %mean( colvec( hsvCent(:,:,2) ) )';
        %mean( colvec( hsvCent(:,:,3) ) )';
        %var( colvec( hsvCent(:,:,1) ) )';
        %var( colvec( hsvCent(:,:,2) ) )';
        %var( colvec( hsvCent(:,:,3) ) )';
        %hist( colvec( hsvCent(:,:,1) ), 7 )';
        %hist( colvec( hsvCent(:,:,2) ) )';
        hist( colvec( hsvCent(:,:,3) ) )';
        
        size( step( face, hsv ), 1 );
        size( step( face, hsvCent ), 1 );
        
        %mae( colvec( diff( hsv(:,:,3) ) ) );
        %mae( colvec( diff( hsvCent(:,:,3) ) ) );
        ];

% Create Feature Vector
Z = zeros(140,140,3);
f = zeros( size( feat( Z, Z ), 1 ), size( I, 1 ) );
parfor i = 1:length( I )
    % Find non-spatial features
    f(:,i) = feat( HSV{i}, imcrop( HSV{i}, [1/3 1/3 1/3 1/3] * 140 ) );
end

% Feature Reduction
fRed = pcaFeat( f, 7 );
feat = @(X,Y) fRed( feat( X, Y ) );
f = fRed( f );

parfor i = 1:length( I )
    % Find non-spatial features
    f(:,i) = feat( HSV{i}, imcrop( HSV{i}, [1/3 1/3 1/3 1/3] * 140 ) );
end

% Initialize Training Variables
N = cell( 1, 5000 );
T = cell( size( N ) );
e = zeros( size( N ) );

% Cross-Validation
disp( 'Training Neural Networks' );
parfor i = 1:length( N )
    disp( ['Neural Network ' num2str( i )] );
    % Train Classifier
    N{i} = feedforwardnet([10 5]);
    [N{i},T{i}] = train( N{i}, f, F' );
    
    % Validation
    e(i) = sqrt( T{i}.best_vperf );
end

mean( e )
min( e )

save('reg1','-v7.3');
