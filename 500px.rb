require 'rubygems'
require 'oauth'
require 'multi_json'

CONSUMER_KEY = '07HQSM9pXpD4DS3kfOnSO862cqoL5ic3txp5lyzd'
CONSUMER_SECRET = 'mWELdfnf7WRIyTPlKGBVKuRsrRF4BvqwWXwqKQwK'
USERNAME = ''
PASSWORD = ''

BASE_URL = 'https://api.500px.com'

def get_access_token
  p "get_access_token: Initializing Consumer" 
  consumer = OAuth::Consumer.new(CONSUMER_KEY, CONSUMER_SECRET, {
  :site               => BASE_URL,
  :request_token_path => "/v1/oauth/request_token",
  :access_token_path  => "/v1/oauth/access_token",
  :authorize_path     => "/v1/oauth/authorize"})

  request_token = consumer.get_request_token()
  p "Request URL: #{request_token.authorize_url}"
  access_token = consumer.get_access_token(request_token, {}, { 
  :x_auth_mode => 'client_auth',
  :x_auth_username => USERNAME,
  :x_auth_password => PASSWORD,
  :scheme => :querystring })
  access_token
end

access_token = get_access_token
p "token: #{access_token.token}" 
p "secret: #{access_token.secret}" 


# Open csv file
F = File.new( 'p.csv', 'w' );

page = 25;
pAt = 0;
for pageAt in 1..page
  #p access_token.get('/v1/photos.json').body
  j = MultiJson.decode(access_token.get('/v1/photos?feature=fresh_week&rpp=100&page=' + pageAt.to_s ).body)
  
  # Iterate each photo
  p = j['photos'];
  for i in 0..p.length-1
    F.write( p[i]['rating'].to_s + "\n" )
    system 'curl -o ' + pAt.to_s + '.jpg ''' + p[i]['image_url'] + ''' &'
    pAt = pAt + 1
  end
end

F.close
