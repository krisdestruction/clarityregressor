function [r,e] = testSvm( S, feat, G )
    % Load Data
    load test;
    G = csvread( '/train/p.csv' );
    
    % Normalize Rating to 0..100
    %F = F - min( G );
    %F = F / max( G ) * 100;
    
    % Create Feature Vector
    Z = zeros(1,1,3);
    f = zeros( size( feat( Z, Z ), 1 ), size( I, 1 ) );
    for i = 1:length( I )
        % Find non-spatial features
        f(:,i) = feat( HSV{i}, imcrop( HSV{i}, [1/3 1/3 1/3 1/3] * 140 ) );
    end
    
    % Test
    e = libsvmclassify( S, f' ) - F;
    r = rms( e );
    %hist( e )
