function [fRed] = pcaFeat( f, p )

    if nargin < 2
        p = size( f, 1 );
    end

    %% Feature Reduction
    % Normalize Features to [-1,1]
    [fNorm,setNorm] = mapminmax( f );
    
    % Perform PCA
    [C,~,e] = pca( fNorm' );
    e / sum( e )
    
    % Truncate PCA to first 18 PC
    fRed = @( X ) ( mapminmax.apply( X, setNorm )' * C(:,1:p) )';
