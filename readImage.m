function [F,I,HSV,face] = readImage( p )
    %% Get Data
    % Get Directory
    d = dir( [p '*.jpg'] );

    % Read Metadata
    F = csvread( [p 'p.csv'] );

    % Remove Zero Ratings
    d = d( F > 0 );
    F = F( F > 0 );

    % Create Face Detector
    face = vision.CascadeObjectDetector;

    % Init Image Cell
    I = cell( size( d ) );
    HSV = cell( size( d ) );
    v = true( size( d ) );

    % Iterate Image Files
    disp( 'Reading Files' );
    parfor i = 1:length( I )
        disp( ['File' num2str( i )] );

        % Read Images
        I{i} = imread( [p d(i).name] );

        % Convert to HSV space
        HSV{i} = rgb2hsv( I{i} );

        if( sum( sum( HSV{i} ) ) == 0 )
            %v(i) = 0;
        end
    end

    % Truncate Invalid Images
    I = I(v);
    HSV = HSV(v);
    F = F(v);

    % Normalize Rating to 0..100
    %F = F - min( F );
    %F = F / max( F ) * 100;
end
