function v = colvec(v)
    %% v = colvec(v)
    % 
    % Inputs
    % v - Vector for vectorization
    % 
    % Outputs
    % v - Vectorized col vector
    
    %% Vectorize
    v = v(:);